<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сайт Дерегуляції</title>
    <link rel="stylesheet" href="./assets/css/common.css" />
    <style>
        .pages {
            margin: 50px 0; 
        }

        .pages ul {
            list-style: none;
            margin: 24px 0;
        }

        .pages ul li {
            font-size: 20px;
            margin-bottom: 6px;
        }

        .pages ul li a {
            font-size: 20px;
            color: #485BC0;
            line-height: 24px;
            text-decoration: none;
            border-bottom: 1px solid #485BC0;
            transition: border .35s ease;
        }

        .pages ul li a:hover {
            border-bottom-color: transparent;
        }
    </style>
</head>
<body> 
<div class="page-wrapper">    
    <main class="main">
        <section class="pages">
            <div class="container">
                <h2 class="h2-title">Список сторінок</h2>
                <ul>
                    <li>
                        <a href="pages/home.php">Home</a>
                    </li>
                    <li>
                        <a href="pages/news.php">News</a>
                    </li>
                    <li>
                        <a href="pages/meetings.php">Meetings</a>
                    </li>
                    <li>
                        <a href="pages/hits.php">Hits</a>
                    </li>
                    <li>
                        <a href="pages/team.php">Team</a>
                    </li>
                    <li>
                        <a href="pages/search.php">Search results</a>
                    </li>
                    <li>
                        <a href="pages/404.php">404</a>
                    </li>
                </ul>
            </div>
        </section>
    </main>
</div>    
</body>
</html>