/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!******************************************!*\
  !*** ./src/scripts/section/hits_list.js ***!
  \******************************************/
window.addEventListener("DOMContentLoaded", function () {
  // Swiper hits
  var swiperHits = new Swiper('.js-swiper-hits', {
    speed: 400,
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
      nextEl: '.js-swiper-button-next',
      prevEl: '.js-swiper-button-prev'
    },
    pagination: {
      el: ".js-swiper-pagination",
      type: 'custom',
      renderCustom: function renderCustom(swiper, current, total) {
        return '<span class="current">' + '0' + current + '</span>' + '/' + '<span class="total">' + '0' + total + '</span>';
      }
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      480: {
        slidesPerView: 2,
        spaceBetween: 20
      },
      767: {
        slidesPerView: 3,
        spaceBetween: 20
      },
      1199: {
        slidesPerView: 4,
        spaceBetween: 30
      }
    }
  });
});
/******/ })()
;