/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/scripts/section/chart.js":
/*!**************************************!*\
  !*** ./src/scripts/section/chart.js ***!
  \**************************************/
/***/ (function() {

var _this = this;
window.addEventListener("DOMContentLoaded", function () {
  // Chart Bar

  // Sample data for the chart
  var data = {
    labels: [['Кількість', 'скасовано'], ['Кількість', 'оптимізовано'], ['Кількість', 'залишено без', 'змін']],
    datasets: [{
      label: 'Міністерство 1',
      data: [105, 38, 70],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 2',
      data: [70, 105, 38],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 3',
      data: [50, 70, 100],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 4',
      data: [80, 70, 60],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 5',
      data: [90, 80, 70],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 6',
      data: [70, 80, 90],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 7',
      data: [60, 70, 80],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 8',
      data: [50, 60, 70],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 9',
      data: [40, 50, 60],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 10',
      data: [30, 40, 50],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 11',
      data: [20, 30, 40],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }, {
      label: 'Міністерство 12',
      data: [10, 20, 30],
      hidden: true,
      backgroundColor: ['rgba(13, 33, 138, 1)', 'rgba(71, 91, 192, 1)', 'rgba(108, 131, 252, 1)'],
      hoverBackgroundColor: ['rgba(13, 33, 138, 0.8)', 'rgba(71, 91, 192, 0.8)', 'rgba(108, 131, 252, 0.8)']
    }]
  };

  // Configuration options for the chart
  var options = {
    scales: {
      x: {
        ticks: {
          font: {
            family: 'e-Ukraine',
            size: 12,
            lineHeight: 1.3,
            weight: 300
          }
        }
      },
      y: {
        ticks: {
          font: {
            size: 12,
            family: 'e-Ukraine'
          }
        }
      }
    },
    plugins: {
      legend: {
        display: false
      }
    }
  };

  // Get the canvas element
  var ctx = document.getElementById('barChart').getContext('2d');

  // Create the styled bar chart
  var barChart = new Chart(ctx, {
    type: 'bar',
    data: data,
    options: options
  });

  // Update chart
  var checkboxList = document.getElementsByClassName('js-checkbox-btn');
  var checkboxListAll = document.querySelector('.js-chart-checkbox_all');
  checkboxListAll.addEventListener('change', function () {
    Array.from(checkboxList).forEach(function (checkbox) {
      checkbox.checked = this.checked;
      !this.checked ? barChart.hide(checkbox.value) : barChart.show(checkbox.value);
    }, this);
  });
  Array.from(checkboxList).forEach(function (item) {
    item.addEventListener('change', function () {
      updateChart(this);
    });
  }, _this);
  function updateChart(checkbox) {
    var currentVal = checkbox.value;
    var isDataShown = barChart.isDatasetVisible(currentVal);
    isDataShown == false ? barChart.show(currentVal) : barChart.hide(currentVal);
  }
});

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/scripts/section/chart.js"]();
/******/ 	
/******/ })()
;