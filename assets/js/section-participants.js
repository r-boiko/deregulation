/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*********************************************!*\
  !*** ./src/scripts/section/participants.js ***!
  \*********************************************/
window.addEventListener("DOMContentLoaded", function () {
  // Swiper participants
  var swiperParticipants = new Swiper('.js-swiper-participants', {
    speed: 400,
    slidesPerView: 6,
    spaceBetween: 30,
    pagination: {
      el: '.js-swiper-pagination',
      clickable: true
    },
    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 15
      },
      480: {
        slidesPerView: 3,
        spaceBetween: 15
      },
      767: {
        slidesPerView: 4,
        spaceBetween: 20
      },
      980: {
        slidesPerView: 5,
        spaceBetween: 30
      },
      1200: {
        slidesPerView: 6,
        spaceBetween: 30
      }
    }
  });
});
/******/ })()
;