/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!**********************************************!*\
  !*** ./src/scripts/section/meetings_list.js ***!
  \**********************************************/
window.addEventListener("DOMContentLoaded", function () {
  var meetingsJsonData = [{
    "name": "Засідання 1",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 1",
    "link": "#"
  }, {
    "name": "Засідання 2",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 2",
    "link": "#"
  }, {
    "name": "Засідання 3",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 3",
    "link": "#"
  }, {
    "name": "Засідання 4",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 4",
    "link": "#"
  }, {
    "name": "Засідання 5",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 5",
    "link": "#"
  }, {
    "name": "Засідання 6",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 6",
    "link": "#"
  }, {
    "name": "Засідання 7",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 7",
    "link": "#"
  }, {
    "name": "Засідання 8",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 8",
    "link": "#"
  }, {
    "name": "Засідання 9",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 9",
    "link": "#"
  }, {
    "name": "Засідання 10",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 10",
    "link": "#"
  }, {
    "name": "Засідання 11",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 11",
    "link": "#"
  }, {
    "name": "Засідання 12",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 12",
    "link": "#"
  }, {
    "name": "Засідання 13",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 13",
    "link": "#"
  }, {
    "name": "Засідання 14",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 14",
    "link": "#"
  }, {
    "name": "Засідання 15",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 15",
    "link": "#"
  }, {
    "name": "Засідання 16",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 16",
    "link": "#"
  }, {
    "name": "Засідання 17",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 17",
    "link": "#"
  }, {
    "name": "Засідання 18",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 18",
    "link": "#"
  }, {
    "name": "Засідання 19",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 19",
    "link": "#"
  }, {
    "name": "Засідання 20",
    "date": "22.02.2023",
    "authority": "МОЗ, Мінфін 20",
    "link": "#"
  }];
  var tbody = document.querySelector('.js-tbody');
  var inputSearch = document.querySelector('.js-input-search');
  function createTable(data) {
    data.forEach(function (item) {
      return tbody.insertAdjacentHTML('afterbegin', "<tr>\n                <td>".concat(item.name, "</td>\n                <td>").concat(item.date, "</td>\n                <td>").concat(item.authority, "</td>\n                <td>\n                    <a href=\"").concat(item.link, "\">\n                        <svg class=\"icon\">\n                            <use href=\"../assets/icons/sprite.svg#show\"></use>\n                        </svg>\n                    </a>\n                </td>\n            </tr>"));
    });
  }
  createTable(meetingsJsonData);
  inputSearch.addEventListener('keyup', function () {
    var searchVal = this.value;
    var filterJsonData = meetingsJsonData.filter(function (item) {
      return item.name === searchVal;
    });
    createTable(filterJsonData);
  });
});
/******/ })()
;