/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!****************************!*\
  !*** ./src/scripts/app.js ***!
  \****************************/
window.addEventListener("DOMContentLoaded", function () {
  var body = document.body;
  var toggleMenu = document.querySelector('.js-toggle-menu');
  toggleMenu.addEventListener('click', function (event) {
    this.classList.toggle('active');
    body.classList.toggle('menu-open');
    event.preventDefault();
  });
});

//If you really need Jquery
/*
$(document).ready(function(){

})
 */
/******/ })()
;