window.addEventListener("DOMContentLoaded", () => {
// Swiper meeting
    const swiperMeeting = new Swiper('.js-swiper-meeting', {
        speed: 400,
        slidesPerView: 2,
        spaceBetween: 30,
        navigation: {
            nextEl: '.js-swiper-button-next',
            prevEl: '.js-swiper-button-prev',
        },
        pagination: {
            el: ".js-swiper-pagination",
            type: 'custom',
            renderCustom: function (swiper, current, total) {
                return '<span class="current">' + '0' + current + '</span>' + '/' + '<span class="total">' + '0' + total + '</span>'; 
            }
        },
        breakpoints: {
            320: {
                slidesPerView: 1,
                spaceBetween: 15
            },
            767: {
                slidesPerView: 2,
                spaceBetween: 30
            }
        }
    });    
});
