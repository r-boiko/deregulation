<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сайт Дерегуляції</title>
    <?php include('../components/head/_head.php') ?>
    <link rel="stylesheet" href="../assets/css/section-meetings_list.css" />
</head>
<body class="meetings">
<div class="page-wrapper">
    <?php include('../components/header/_header.php') ?>
    <main class="main">
        <div class="container">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item">
                    <a href="#" class="breadcrumbs__link">Головна</a>
                </li>
                <li class="breadcrumbs__item">
                    <svg class="icon">
                        <use href="../assets/icons/sprite.svg#separator"></use>
                    </svg>
                </li>
                <li class="breadcrumbs__item">Засідання</li>   
            </ul>
            <h1 class="h1-title">Засідання</h1>
            <?php include('../components/sections/meeting_list.php') ?>
        </div> 
    </main>
    <?php include('../components/footer/_footer.php') ?>   
</div>    
<?php include('../components/scripts/_scripts.php') ?>
<script type="text/javascript" src="../assets/js/section-meetings_list.js"></script>
</body>
</html>