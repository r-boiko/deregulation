<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сайт Дерегуляції</title>
    <?php include('../components/head/_head.php') ?>
    <link rel="stylesheet" href="../assets/css/section-search_results.css" />
</head>
<body class="search">
<div class="page-wrapper">
    <?php include('../components/header/_header.php') ?>
    <main class="main">
        <?php include('../components/sections/search_results.php') ?>
    </main>
    <?php include('../components/footer/_footer.php') ?>   
</div>    
<?php include('../components/scripts/_scripts.php') ?>
</body>
</html>