<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сайт Дерегуляції</title>
    <?php include('../components/head/_head.php') ?>
    <link rel="stylesheet" href="../assets/libs/swiper/swiper.min.css" />
    <link rel="stylesheet" href="../assets/css/section-hero.css" />
    <link rel="stylesheet" href="../assets/css/section-step.css" />
    <link rel="stylesheet" href="../assets/css/section-result.css" />
    <link rel="stylesheet" href="../assets/css/section-meeting.css" />
    <link rel="stylesheet" href="../assets/css/section-chart.css" />
    <link rel="stylesheet" href="../assets/css/section-hits.css" />
    <link rel="stylesheet" href="../assets/css/section-participants.css" />
    <link rel="stylesheet" href="../assets/css/section-news.css" />
</head>
<body class="home">
<div class="page-wrapper">
    <?php include('../components/header/_header.php') ?>
    <main class="main">
        <?php include('../components/sections/hero.php') ?>
        <?php include('../components/sections/step.php') ?>
        <?php include('../components/sections/result.php') ?>
        <?php include('../components/sections/meeting.php') ?>
        <?php include('../components/sections/chart.php') ?>
        <?php include('../components/sections/hits.php') ?>
        <?php include('../components/sections/participants.php') ?>
        <?php include('../components/sections/news.php') ?>
    </main>
    <?php include('../components/footer/_footer.php') ?>   
</div>    
<?php include('../components/scripts/_scripts.php') ?>
<script type="text/javascript" src="../assets/libs/swiper/swiper.min.js"></script>
<script type="text/javascript" src="../assets/libs/chart/chart.js"></script>
<script type="text/javascript" src="../assets/js/section-meeting.js"></script>
<script type="text/javascript" src="../assets/js/section-hits.js"></script>
<script type="text/javascript" src="../assets/js/section-participants.js"></script>
<script type="text/javascript" src="../assets/js/section-news.js"></script>
<script type="text/javascript" src="../assets/js/section-chart.js"></script>
</body>
</html>