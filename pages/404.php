<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сайт Дерегуляції</title>
    <?php include('../components/head/_head.php') ?>
    <link rel="stylesheet" href="../assets/css/section-404.css" />
</head>
<body class="errors-404">
<div class="page-wrapper">
    <?php include('../components/header/_header.php') ?>
    <main class="main">
        <?php include('../components/sections/404.php') ?>
    </main>
    <?php include('../components/footer/_footer.php') ?>   
</div>    
<?php include('../components/scripts/_scripts.php') ?>
</body>
</html>