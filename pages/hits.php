<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Сайт Дерегуляції</title>
    <?php include('../components/head/_head.php') ?>
    <link rel="stylesheet" href="../assets/libs/swiper/swiper.min.css" />
    <link rel="stylesheet" href="../assets/css/section-hits_list.css" />
</head>
<body class="hits">
<div class="page-wrapper">
    <?php include('../components/header/_header.php') ?>
    <main class="main">
        <?php include('../components/sections/hits__list.php') ?>
    </main>
    <?php include('../components/footer/_footer.php') ?>   
</div>    
<?php include('../components/scripts/_scripts.php') ?>
<script type="text/javascript" src="../assets/libs/swiper/swiper.min.js"></script>
<script type="text/javascript" src="../assets/js/section-hits_list.js"></script>
</body>
</html>