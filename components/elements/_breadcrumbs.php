<ul class="breadcrumbs">
    <li class="breadcrumbs__item">
        <a href="#" class="breadcrumbs__link">Головна</a>
    </li>
    <li class="breadcrumbs__item">
        <svg class="icon">
            <use href="../assets/icons/sprite.svg#separator"></use>
        </svg>
    </li>
    <li class="breadcrumbs__item">Новини</li>   
</ul>  