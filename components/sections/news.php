<!-- section-news -->
<section class="news">	
	<div class="container">
		<h2 class="h2-title">Новини</h2>
		<div class="swiper js-swiper-new">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<a href="#" class="new-card">
						<div class="new-card__image">
							<img src="../assets/img/new-image.png" alt="">
						</div>
						<div class="new-card__text">
							<div class="new-card__date">23 грудня 2022</div>
							<h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
						</div>
					</a>
				</div>
				<div class="swiper-slide">
					<a href="#" class="new-card">
						<div class="new-card__image">
							<img src="../assets/img/new-image.png" alt="">
						</div>
						<div class="new-card__text">
							<div class="new-card__date">23 грудня 2022</div>
							<h3 class="h3-title">Відкриті дані для органів місцевого самоврядування</h3>
						</div>
					</a>
				</div>
				<div class="swiper-slide">
					<a href="#" class="new-card">
						<div class="new-card__image">
							<img src="../assets/img/new-image.png" alt="">
						</div>
						<div class="new-card__text">
							<div class="new-card__date">23 грудня 2022</div>
							<h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
						</div>
					</a>
				</div>
				<div class="swiper-slide">
					<a href="#" class="new-card">
						<div class="new-card__image">
							<img src="../assets/img/new-image.png" alt="">
						</div>
						<div class="new-card__text">
							<div class="new-card__date">23 грудня 2022</div>
							<h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
						</div>
					</a>
				</div>
				<div class="swiper-slide">
					<a href="#" class="new-card">
						<div class="new-card__image">
							<img src="../assets/img/new-image.png" alt="">
						</div>
						<div class="new-card__text">
							<div class="new-card__date">23 грудня 2022</div>
							<h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
						</div>
					</a>
				</div>
				<div class="swiper-slide">
					<a href="#" class="new-card">
						<div class="new-card__image">
							<img src="../assets/img/new-image.png" alt="">
						</div>
						<div class="new-card__text">
							<div class="new-card__date">23 грудня 2022</div>
							<h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
						</div>
					</a>
				</div>
			</div>

			<div class="news__bottom">
				<div class="news__arrows">
					<div class="swiper-button-prev js-swiper-button-prev">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#prev"></use>
						</svg>
					</div>
					<div class="swiper-pagination js-swiper-pagination"></div>
					<div class="swiper-button-next js-swiper-button-next">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#next"></use>
						</svg>
					</div>
				</div>
				<a href="#" class="news__all">Більше новин
					<svg class="icon">
						<use href="../assets/icons/sprite.svg#right"></use>
					</svg>
				</a>
			</div>
		</div>
	</div>
</section>
<!-- end news -->
