<!-- section-result -->
<section class="result">	
	<div class="container">
		<div class="result__content">
			<h2 class="section-title">Загальні цифри результатів роботи МРГ</h2>
			<div class="list">
				<div class="list__item">
					<div class="list__number">34</div>
					<p>Кількість засідань</p>
				</div>
				<div class="list__item">
					<div class="list__number">34,678</div>
					<p>Кількість скасованих інструментів</p>
				</div>
				<div class="list__item">
					<div class="list__number">34</div>
					<p>Кількість інструментів, що будуть оцифровані</p>
				</div>
				<div class="list__item">
					<div class="list__number">34,567<span>грн</span></div>
					<p>Потенційний грошовий результат цих змін для укр. бюджету</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- end section-result -->
