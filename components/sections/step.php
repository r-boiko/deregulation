<!-- section-step -->
<section class="step">
	<div class="container">
		<div class="step__head">
			<h2 class="h2-title">Кроки</h2>
			<div class="sub-title">Якийсь короткий текст було б добре сюди вставити!</div>
		</div>
		<div class="list">
			<div class="list__item">
				<div class="list__number">01</div>
				<h4 class="h4-title">Назва коротка</h4>
				<p>Lorem ipsum dolor sit amet consectetur. Natoque congue est sapien sed montes et. Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
			</div>
			<div class="list__item">
				<div class="list__number">02</div>
				<h4 class="h4-title">Назва коротка</h4>
				<p>Lorem ipsum dolor sit amet consectetur. Natoque congue est sapien sed montes et. Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
			</div>
			<div class="list__item">
				<div class="list__number">03</div>
				<h4 class="h4-title">Назва коротка</h4>
				<p>Lorem ipsum dolor sit amet consectetur. Natoque congue est sapien sed montes et. Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
			</div>
		</div>
	</div>
</section>
<!-- end section-step -->
