<!-- section-404 -->
<section class="error-404">
    <div class="container">
        <h3 class="h3-title">Дерегуляційна реформа</h3>
        <div class="error-404__image">
            <img src="../assets/img/error-404.png" alt="">
        </div>
        <h2 class="h2-title">Вибачте! Такої сторінки не існує!</h2>
        <p>Ви можете повернутися на головну сторінку і продовжити свої пошуки</p>
        <a href="#" class="error-404__btn">На головну сторінку</a>
    </div>
</section>
<!-- end section-404 -->
