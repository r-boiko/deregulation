<!-- section-meeting_list -->
<section class="meeting-list">
    <div class="meeting-list__head">
        <select name="" id="">
            <option>Залучені органи 1</option>
            <option>Залучені органи 2</option>
            <option>Залучені органи 3</option>
            <option>Залучені органи 4</option>
            <option>Залучені органи 5</option>
        </select>
        <input type="text" class="js-input-search" placeholder="Пошук засідання">
    </div>
    <div class="meeting-list__content">
        <table>
            <thead>
                <tr>
                    <th>Назва засідання</th>
                    <th>Дата</th>
                    <th>Залучені органи</th>
                    <th></th>
                </tr>
            </thead>
            <tbody class="js-tbody">
                
            </tbody>
        </table>
    </div>
    <div class="meeting-list__bottom">
        <div class="pagination">
            <a href="#" class="pagination__prev">Попередня сторінка
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#left"></use>
                </svg>
            </a>
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link pagination__link_active">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">4</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">5</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">···</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">35</a>
                </li>
            </ul>
            <a href="#" class="pagination__next">Наступна сторінка
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#right"></use>
                </svg>
            </a>
        </div>
    </div>
</section>
<!-- end section-meeting_list -->
