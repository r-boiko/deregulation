<!-- section-news_list -->
<section class="news-list">
    <div class="container">
        <ul class="breadcrumbs">
            <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link">Головна</a>
            </li>
            <li class="breadcrumbs__item">
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#separator"></use>
                </svg>
            </li>
            <li class="breadcrumbs__item">Новини</li>   
        </ul>  
        <h1 class="h1-title">Новини</h1>
        <div class="news-list__wrapper">
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Відкриті дані для органів місцевого самоврядування</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>

            <hr>

            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Відкриті дані для органів місцевого самоврядування</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>

            <hr>

            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Відкриті дані для органів місцевого самоврядування</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>

            <hr>
            
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Відкриті дані для органів місцевого самоврядування</h3>
                </div>
            </a>
            <a href="#" class="new-card">
                <div class="new-card__image">
                    <img src="../assets/img/new-image.png" alt="">
                </div>
                <div class="new-card__text">
                    <div class="new-card__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                </div>
            </a>
        </div>
        <div class="pagination">
            <a href="#" class="pagination__prev">Попередня сторінка
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#left"></use>
                </svg>
            </a>
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link pagination__link_active">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">4</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">5</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">···</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">35</a>
                </li>
            </ul>
            <a href="#" class="pagination__next">Наступна сторінка
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#right"></use>
                </svg>
            </a>
        </div>
    </div>
</section>
<!-- end section-news_list -->
