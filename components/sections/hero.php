<!-- section-hero -->
<section class="hero" style="background-image: url(../assets/img/ukraine-waving-flag-beautiful-sky.png)">
	<div class="container">
		<div class="hero__text">
			<h1 class="section-title">Мета реформи</h1>
			<p>Lorem ipsum dolor sit amet consectetur. Natoque congue est sapien sed montes et. Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus. Id elit malesuada gravida egestas adipiscing nisi eget parturient dui. Diam imperdiet mi molestie eu.</p>
			<p>Malesuada amet volutpat lorem tempor eget pellentesque senectus. In massa tempus etiam eros neque cursus pulvinar interdum tortor.</p>
		</div>
	</div>
</section>
<!-- end section-hero -->
