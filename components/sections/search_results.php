<!-- section-search_results -->
<section class="search_results">
    <div class="container">
        <ul class="breadcrumbs">
            <li class="breadcrumbs__item">
                <a href="#" class="breadcrumbs__link">Головна</a>
            </li>
            <li class="breadcrumbs__item">
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#separator"></use>
                </svg>
            </li>
            <li class="breadcrumbs__item">Пошук</li>   
        </ul>
        <h1 class="h1-title">За запитом “дерегуляція” знайдено 44 результати</h1>    
        <div class="results">
            <div class="results__item">
                <div class="results__text">
                    <div class="results__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
                <div class="results__image">
                    <img src="../assets/img/search-results__image.png" alt="">
                </div>
            </div>
            <div class="results__item">
                <div class="results__text">
                    <div class="results__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
                <div class="results__image">
                    <img src="../assets/img/search-results__image.png" alt="">
                </div>
            </div>
            <div class="results__item">
                <div class="results__text">
                    <h3 class="h3-title">Опис реформи</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
            </div>
            <div class="results__item">
                <div class="results__text">
                    <div class="results__date">23 грудня 2022</div>
                    <h3 class="h3-title">Типові помилки акредитації освітньо-професійних програм закладів фахової передвищої освіти</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
                <div class="results__image">
                    <img src="../assets/img/search-results__image.png" alt="">
                </div>
            </div>
            <div class="results__item">
                <div class="results__text">
                    <h3 class="h3-title">Опис реформи</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
            </div>
            <div class="results__item">
                <div class="results__text">
                    <h3 class="h3-title">Опис реформи</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
            </div>
            <div class="results__item">
                <div class="results__text">
                    <h3 class="h3-title">Опис реформи</h3>
                    <p>Lorem ipsum dolor sit amet consectetur. Sed ultricies libero imperdiet est varius amet vestibulum diam. Fermentum porta feugiat morbi vestibulum. Sagittis nulla aenean viverra semper fringilla id. Nec fermentum varius venenatis sed nisl ac quisque pulvinar posuere. Ultricies adipiscing in et adipiscing</p>
                    <a href="#" class="results__link">https://link.com/link/link</a>
                </div>
            </div>
        </div>
        <div class="pagination">
            <a href="#" class="pagination__prev">Попередня сторінка
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#left"></use>
                </svg>
            </a>
            <ul class="pagination__list">
                <li class="pagination__item">
                    <a href="#" class="pagination__link">1</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">2</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link pagination__link_active">3</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">4</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">5</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">···</a>
                </li>
                <li class="pagination__item">
                    <a href="#" class="pagination__link">35</a>
                </li>
            </ul>
            <a href="#" class="pagination__next">Наступна сторінка
                <svg class="icon">
                    <use href="../assets/icons/sprite.svg#right"></use>
                </svg>
            </a>
        </div>
    </div>
</section>
<!-- end section-search_results -->
