<!-- section-meeting -->
<section class="meeting">
	<div class="container">
		<h2 class="h2-title">Наступні засідання</h2>
		<div class="swiper js-swiper-meeting">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="meeting-card">
						<div class="meeting-card__content">
							<div class="meeting-card__text">
								<h4 class="h4-title">Назва засідання 1</h4>
								<p>Lorem ipsum dolor sit amet consectetur.  Natoque congue est sapien sed montes et.  Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
							</div>
							<div class="members">
								<div class="members__caption">Перелік учасників:</div>
								<div class="members__list">Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна, Антонов Антон, Генадій Іваненко, Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна, Антонов Антон, Генадій Іваненко</div>
							</div>
						</div>
						<div class="meeting-card__bottom">
							<div class="date">
								<div class="date__icon">
									<svg class="icon">
										<use href="../assets/icons/sprite.svg#calendar"></use>
									</svg>
								</div>
								<div class="date__text">
									<div class="date__title">Дата засідання:</div>
									<div class="date__number">10.10.2023</div>
								</div>
							</div>
							<a href="#" class="meeting-card__more">Дізнатись більше
								<svg class="icon">
									<use href="../assets/icons/sprite.svg#more"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="meeting-card">
						<div class="meeting-card__content">
							<div class="meeting-card__text">
								<h4 class="h4-title">Назва засідання 2</h4>
								<p>Lorem ipsum dolor sit amet consectetur.  Natoque congue est sapien sed montes et.  Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
							</div>
							<div class="members">
								<div class="members__caption">Перелік учасників:</div>
								<div class="members__list">Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна</div>
							</div>
						</div>
						<div class="meeting-card__bottom">
							<div class="date">
								<div class="date__icon">
									<svg class="icon">
										<use href="../assets/icons/sprite.svg#calendar"></use>
									</svg>
								</div>
								<div class="date__text">
									<div class="date__title">Дата засідання:</div>
									<div class="date__number">10.10.2023</div>
								</div>
							</div>
							<a href="#" class="meeting-card__more">Дізнатись більше
								<svg class="icon">
									<use href="../assets/icons/sprite.svg#more"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="meeting-card">
						<div class="meeting-card__content">
							<div class="meeting-card__text">
								<h4 class="h4-title">Назва засідання 1</h4>
								<p>Lorem ipsum dolor sit amet consectetur.  Natoque congue est sapien sed montes et.  Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
							</div>
							<div class="members">
								<div class="members__caption">Перелік учасників:</div>
								<div class="members__list">Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна, Антонов Антон, Генадій Іваненко, Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна, Антонов Антон, Генадій Іваненко</div>
							</div>
						</div>
						<div class="meeting-card__bottom">
							<div class="date">
								<div class="date__icon">
									<svg class="icon">
										<use href="../assets/icons/sprite.svg#calendar"></use>
									</svg>
								</div>
								<div class="date__text">
									<div class="date__title">Дата засідання:</div>
									<div class="date__number">10.10.2023</div>
								</div>
							</div>
							<a href="#" class="meeting-card__more">Дізнатись більше
								<svg class="icon">
									<use href="../assets/icons/sprite.svg#more"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="meeting-card">
						<div class="meeting-card__content">
							<div class="meeting-card__text">
								<h4 class="h4-title">Назва засідання 2</h4>
								<p>Lorem ipsum dolor sit amet consectetur.  Natoque congue est sapien sed montes et.  Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
							</div>
							<div class="members">
								<div class="members__caption">Перелік учасників:</div>
								<div class="members__list">Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна</div>
							</div>
						</div>
						<div class="meeting-card__bottom">
							<div class="date">
								<div class="date__icon">
									<svg class="icon">
										<use href="../assets/icons/sprite.svg#calendar"></use>
									</svg>
								</div>
								<div class="date__text">
									<div class="date__title">Дата засідання:</div>
									<div class="date__number">10.10.2023</div>
								</div>
							</div>
							<a href="#" class="meeting-card__more">Дізнатись більше
								<svg class="icon">
									<use href="../assets/icons/sprite.svg#more"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="meeting-card">
						<div class="meeting-card__content">
							<div class="meeting-card__text">
								<h4 class="h4-title">Назва засідання 1</h4>
								<p>Lorem ipsum dolor sit amet consectetur.  Natoque congue est sapien sed montes et.  Pulvinar sit enim ullamcorper hendrerit pretium. Amet semper risus ipsum maecenas eget cursus.</p>
							</div>
							<div class="members">
								<div class="members__caption">Перелік учасників:</div>
								<div class="members__list">Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна, Антонов Антон, Генадій Іваненко, Антонов Антон, Генадій Іваненко, Патрік Джонсонюк, Мирослава Мирославна, Антонов Антон, Генадій Іваненко</div>
							</div>
						</div>
						<div class="meeting-card__bottom">
							<div class="date">
								<div class="date__icon">
									<svg class="icon">
										<use href="../assets/icons/sprite.svg#calendar"></use>
									</svg>
								</div>
								<div class="date__text">
									<div class="date__title">Дата засідання:</div>
									<div class="date__number">10.10.2023</div>
								</div>
							</div>
							<a href="#" class="meeting-card__more">Дізнатись більше
								<svg class="icon">
									<use href="../assets/icons/sprite.svg#more"></use>
								</svg>
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="meeting__bottom">
				<div class="meeting__arrows">
					<div class="swiper-button-prev js-swiper-button-prev">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#prev"></use>
						</svg>
					</div>
					<div class="swiper-pagination js-swiper-pagination"></div>
					<div class="swiper-button-next js-swiper-button-next">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#next"></use>
						</svg>
					</div>
				</div>
				<a href="#" class="meeting__all">Дивитись всі засідання
					<svg class="icon">
						<use href="../assets/icons/sprite.svg#right"></use>
					</svg>
				</a>
			</div>
		</div>
	</div> 
</section>

<!-- end section-meeting -->
