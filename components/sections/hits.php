<!-- section-hits -->
<section class="hits">	
	<div class="container">
		<h2 class="h2-title">Хіти</h2>
		<div class="swiper js-swiper-hits">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orc</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту в дві строки</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orc</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orc</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orc</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orc</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="hit-card">
						<h4 class="h4-title">Назва інструменту</h4>
						<div class="hit-card__label">Було</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orc</p>
						<div class="hit-card__label hit-card__label_blue">Стало</div>
						<p>Lorem ipsum dolor sit amet consectetur. Orci nulla est cras sed.</p>
					</div>
				</div>
			</div>
			<div class="hits__bottom">
				<div class="hits__arrows">
					<div class="swiper-button-prev js-swiper-button-prev">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#prev"></use>
						</svg>
					</div>
					<div class="swiper-pagination js-swiper-pagination"></div>
					<div class="swiper-button-next js-swiper-button-next">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#next"></use>
						</svg>
					</div>
				</div>
				<a href="#" class="hits__all">Дивитись всі хіти
					<svg class="icon">
						<use href="../assets/icons/sprite.svg#right"></use>
					</svg>
				</a>
			</div>
		</div>
	</div>
</section>
<!-- end section-hits -->
