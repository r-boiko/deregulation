<!-- section-participants -->
<section class="participants">	
	<div class="container">
		<hr>
		<h2 class="section-title">Хто залучений</h2> 
		<div class="swiper js-swiper-participants">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_1.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Держлікслужба</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_1.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Держлікслужба</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_1.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Держлікслужба</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_2.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Міністерство з питань реінтеграції тимчасово окупованих територій України</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_3.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Державна служба України з питань праці</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_1.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Держлікслужба</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_1.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Держлікслужба</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div class="participant-card">
						<div class="participant-card__logo">
							<img src="../assets/img/partner-image_1.png" alt="">
						</div>
						<div class="participant-card__text">
							<div class="participant-card__title">Держлікслужба</div>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-pagination js-swiper-pagination"></div>
		</div>	
	</div>
</section>
<!-- end section-participants -->
