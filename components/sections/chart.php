<!-- section-chart -->
<section class="chart">	 
	<div class="container">
		<h2 class="h2-title">Графік опрацьованих інструментів</h2>
		<div class="chart__content">
			<div class="chart__text">
				<div class="list">
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-chart-checkbox_all" id="chart-checkbox_all" value="all">
						<label for="chart-checkbox_all">
							<strong>Вибрати всі міністерства</strong>
						</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_1" value="0">
						<label for="chart-checkbox_1">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_2" value="1">
						<label for="chart-checkbox_2">Міністерство інфраструктури України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_3" value="2">
						<label for="chart-checkbox_3">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_4" value="3">
						<label for="chart-checkbox_4">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_5" value="4">
						<label for="chart-checkbox_5">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_6" value="5">
						<label for="chart-checkbox_6">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_7" value="6">
						<label for="chart-checkbox_7">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_8" value="7">
						<label for="chart-checkbox_8">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_9" value="8">
						<label for="chart-checkbox_9">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_10" value="9">
						<label for="chart-checkbox_10">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_11" value="10">
						<label for="chart-checkbox_11">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
					<div class="list__item">
						<input type="checkbox" class="checkbox-btn js-checkbox-btn" id="chart-checkbox_12" value="11">
						<label for="chart-checkbox_12">Міністерство розвитку економіки, торгівлі та сільського господарства України</label>
					</div>
				</div>
			</div>
			<div class="bar">
				<canvas id="barChart"
						width="326"
						height="237">
				</canvas>
			</div>
		</div>
	</div>
</section>
<!-- end section-chart --> 
