<!-- section-team -->
<section class="team">
    <div class="team__head" style="background-image: url(../assets/img/team-bg.png)">
        <div class="team__blur" style="background-image: url(../assets/img/team-blur.svg)"></div>
        <div class="container">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item">
                    <a href="#" class="breadcrumbs__link">Головна</a>
                </li>
                <li class="breadcrumbs__item">
                    <svg class="icon">
                        <use href="../assets/icons/sprite.svg#separator"></use>
                    </svg>
                </li>
                <li class="breadcrumbs__item">Наша команда</li>   
            </ul>
            <div class="team__text">
                <h1 class="h1-title">Наша команда</h1>
                <p>Lorem ipsum dolor sit amet consectetur. Natoque congue est sapien sed montes et. Pulvinar sit enim ullamcorper hendrerit pretium.</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="team__wrapper">
            <div class="team__nav">
                <a href="#" class="team__link">Міністерство економіки України</a>
                <a href="#" class="team__link team__link_active">Міністерство цифрової трансформації України</a>
                <a href="#" class="team__link">Офіс ефективного регулювання BRDO</a>
                <a href="#" class="team__link">Офіс 1</a>
                <a href="#" class="team__link">Офіс 2</a>
            </div>
            <div class="team__content">
                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>
                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>
                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>

                <hr>

                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>
                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>
                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>

                <hr>

                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>
                <div class="team-card">
                    <div class="team-card__photo">
                        <img src="../assets/img/team-photo.png" alt="">
                    </div>
                    <div class="team-card__text">
                        <h4 class="h4-title">Ганна Василівна Василенко</h4>
                        <span class="team-card__position">назва посади</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- end section-team -->
