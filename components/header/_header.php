<!-- header -->
<header class="header">
	<div class="header__top">
		<div class="container">
			<div class="header__left">
				<div class="header__title">Дерегуляційна реформа</div>
				<ul class="contact">
					<li class="contact__item">
						<svg class="icon contact__icon">
							<use href="../assets/icons/sprite.svg#phone"></use>
						</svg>
						<a href="tel:380937386480" class="contact__link">+380937386480</a>
					</li>
					<li class="contact__item">
						<svg class="icon contact__icon">
							<use href="../assets/icons/sprite.svg#email"></use>
						</svg>
						<a href="mailto:email@email.com" class="contact__link">email@email.com</a>
					</li>
				</ul>
			</div>
			<a href="/" class="header__logo">
				<img src="../assets/img/logo.svg" alt="">
			</a>
			<div class="toggle-menu js-toggle-menu">
                <span />  
            </div>
		</div>
	</div>
	<div class="header__bottom">
		<div class="container">
			<nav class="nav">
				<ul class="nav__list">
					<li class="nav__item">
						<a href="#" class="nav__link nav__link_active">Про Реформу</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Розроблені продукти</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Засідання</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Новини</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Регуляторний дашборд</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Хіти</a>
					</li>
				</ul>
			</nav>
			<div class="header__btn">
				<svg class="icon">
					<use href="../assets/icons/sprite.svg#search"></use>
				</svg>
				Пошук по сайту
			</div>
		</div>
	</div>
	<!-- mob-nav -->
	<div class="mobNav js-mobNav">
		<div class="header__title">Дерегуляційна реформа</div>
		<nav class="nav">
			<ul class="nav__list">
				<li class="nav__item">
					<a href="#" class="nav__link nav__link_active">Про Реформу</a>
				</li>
				<li class="nav__item">
					<a href="#" class="nav__link">Розроблені продукти</a>
				</li>
				<li class="nav__item">
					<a href="#" class="nav__link">Засідання</a>
				</li>
				<li class="nav__item">
					<a href="#" class="nav__link">Новини</a>
				</li>
				<li class="nav__item">
					<a href="#" class="nav__link">Регуляторний дашборд</a>
				</li>
				<li class="nav__item">
					<a href="#" class="nav__link">Хіти</a>
				</li>
			</ul>
		</nav>
		<ul class="contact">
			<li class="contact__item">
				<svg class="icon contact__icon">
					<use href="../assets/icons/sprite.svg#phone"></use>
				</svg>
				<a href="tel:380937386480" class="contact__link">+380937386480</a>
			</li>
			<li class="contact__item">
				<svg class="icon contact__icon">
					<use href="../assets/icons/sprite.svg#email"></use>
				</svg>
				<a href="mailto:email@email.com" class="contact__link">email@email.com</a>
			</li>
		</ul>
	</div>
	<!-- end mob-nav -->
</header>

<!-- end header -->
