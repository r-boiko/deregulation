<!-- footer -->
<footer class="footer">
	<div class="container">
		<div class="footer__top">
			<div class="partner">
				<div class="partner__title">партнери</div>
				<ul class="partner__list">
					<li class="partner__item">
						<img src="../assets/img/footer-partner-logo_1.png" alt="">
					</li>
					<li class="partner__item">
						<img src="../assets/img/footer-partner-logo_2.png" alt="">
					</li>
					<li class="partner__item"> 
						<img src="../assets/img/footer-partner-logo_3.png" alt="">
					</li>
					<li class="partner__item">
						<img src="../assets/img/footer-partner-logo_4.png" alt="">
					</li>
				</ul>
				<p>Developed by the Project EU4Business: SME Policies and Institutions Support (SMEPIS) Implemented by Ecorys, in consortium with GIZ, BRDO and Civitta</p>
			</div>
			<nav class="nav">
				<div class="nav__title">сайт</div>
				<ul class="nav__list">
					<li class="nav__item">
						<a href="#" class="nav__link">Про Реформу</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Розроблені продукти</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Засідання</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Новини</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Регуляторний дашборд</a>
					</li>
					<li class="nav__item">
						<a href="#" class="nav__link">Хіти</a>
					</li>
				</ul>
			</nav>
			<div class="contact">
				<div class="contact__title">контакти</div>
				<ul class="list">
					<li class="list__item">
						<a href="tel:380937386480" class="list__link list__link_phone">+380937386480</a>
					</li>
					<li class="list__item">
						<a href="mailto:email@email.com" class="list__link">email@email.com</a>
					</li>
					<li class="list__item">01008, Україна, м. Київ, вул. Грушевського, 12/2</li>
				</ul>
				<div class="soc">
					<a href="#" class="soc__link">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#facebook"></use>
						</svg>
					</a>
					<a href="#" class="soc__link">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#linkedin"></use>
						</svg>
					</a>
					<a href="#" class="soc__link">
						<svg class="icon">
							<use href="../assets/icons/sprite.svg#twitter"></use>
						</svg>
					</a>
				</div>
				<a href="#" class="contact__btn">Напишіть нам</a>
			</div>
		</div>
		<div class="footer__bottom">
			<div class="footer__copyright">© Дерегуляційна реформа | 2023</div>
			<div class="footer__privacy">
				<a href="#">Правила користування сайтом</a>
				<a href="#">Публічна аферта</a>
				<a href="#">Privacy Policy</a>
			</div>
		</div>
	</div>
</footer>
<!-- end footer -->
